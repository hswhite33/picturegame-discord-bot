import * as Discord from 'discord.js';

import * as Bot from '../model/bot';
import { State } from '../model/state';

import * as config from '../../config.json';


export const miscCommands: Bot.CommandSpec = {
    description: 'Misc',
    commands: [
        {
            command: 'hi',
            description: 'Says hello to whoever made the command',
            reply: true,
            execute: () => async send => {
                await send('hello there!');
            },
        },
        {
            command: 'celebrate',
            description: 'Celebrate with the group',
            execute: () => async send => {
                const gifs = config.bot.celebrateGifs;
                if (!gifs) {
                    await send('🎉🎉🎉 Woohoo! 🎉🎉🎉');
                    return;
                }

                const embed: Discord.MessageEmbedOptions = {
                    image: {
                        url: gifs[Math.floor(Math.random() * gifs.length)],
                    },
                };
                await send({ embed });
            },
        },
        {
            command: 'sing',
            description: `Hear some lyrics from ${config.discord.botName}'s favorite song`,
            execute: () => async send => {
                await send(
                    ':musical_note: *I see you when you\'re posting... ' +
                    'I know when you +correct... ' +
                    'I know if your image is RIS-able... ' +
                    'No round will go unchecked!* :musical_note:');
            },
        },
        {
            command: 'about',
            description: `Returns details on the current version of ${config.discord.botName}`,
            execute: () => async send => {
                const state = State.getState();
                const versionInfo = await state.api.getStatus();

                const response = versionInfo.services.map(s => {
                    return `**${s.name}**: version ${s.version}`;
                }).join('\n');
                await send(response);
            },
        },
    ],
};

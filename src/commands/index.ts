import * as Discord from 'discord.js';
import * as winston from 'winston';

import * as Bot from '../model/bot';
import { State } from '../model/state';
import * as utils from '../utils';

import { adminCommands } from './admin';
import { currentRoundCommands } from './currentRound';
import { madlistCommands } from './madlist';
import { categories, metaCommands } from './meta';
import { miscCommands } from './misc';
import { rulesCommands } from './rules';
import { statsCommands } from './stats';
import { subscribeCommands } from './subscribe';

import * as config from '../../config.json';

export const allCommands: Bot.Command[] = [];

export async function initialize() {
    const state = State.getState();
    const commandMap = new Map<string, Bot.Command>();

    register(currentRoundCommands, commandMap, '1⃣');
    register(statsCommands, commandMap, '2⃣');
    register(subscribeCommands, commandMap, '3⃣');
    register(rulesCommands, commandMap, '4⃣');
    register(madlistCommands, commandMap, '5⃣');
    register(metaCommands, commandMap, '6⃣');
    register(miscCommands, commandMap, '7⃣');
    register(adminCommands, commandMap);

    config.bot.commands?.forEach(commandConf => {
        const command = commandMap.get(commandConf.name);
        if (!command) { return; }

        if (commandConf.roles) {
            command.permissions = new Set(commandConf.roles.map(r => r.toLowerCase()));
        }
        command.cooldown = commandConf.cooldown;

        if (commandConf.allowedChannels) {
            command.allowedChannels = new Set(
                commandConf.allowedChannels.map(c => c.toLowerCase()));
        } else {
            command.forbiddenChannels = new Set(
                commandConf.forbiddenChannels?.map(c => c.toLowerCase()));
        }

        commandConf.aliases?.forEach(alias => commandMap.set(alias, command));
        command.aliases = commandConf.aliases;
    });

    state.commands = commandMap;
    await state.reportStartup();
}

function register(spec: Bot.CommandSpec, commandMap: Map<string, Bot.Command>, category?: string) {
    spec.commands.forEach(command => {
        allCommands.push(command);
        commandMap.set(command.command, command);
    });

    if (category) {
        categories.set(category, spec);
    }
}

export async function handleCommand(message: Discord.Message, command: string) {
    const state = State.getState();
    const params = command.split(/\s+/);
    const commandString = utils.sanitizeWord(params[0]);
    const com = state.getCommand(commandString);

    if (!com) {
        await message.reply(utils.commandNotFound(commandString, message));
        return;
    }

    const permission = hasPermission(message.author, com);

    const channelId = message.channel.id;
    const currentTime = new Date();
    const cooldownState = state.data.commandCooldowns.get(com.command) ?? {};
    const cooledDown = !cooldownState[channelId]
        || cooldownState[channelId] <= currentTime;

    const channelName = (message.channel as Discord.TextChannel).name;
    const channelAllowed = channelIsAllowed(channelName, com);

    winston.info('Handling command', {
        command: com.command,
        input: commandString,
        params: params.slice(1),
        ...utils.getUserInfoFromMessage(message),
        permission,
        cooledDown,
        channelAllowed,
    });

    let send: Bot.SendMessageCallback = com.reply
        ? message.reply.bind(message)
        : message.channel.send.bind(message.channel);

    const minParams = com.minParameters !== undefined ? com.minParameters
        : (com.parameters ? com.parameters.length : 0);

    let rejectionResponse: Discord.Message | Discord.Message[] | undefined = undefined;

    if (!permission) {
        rejectionResponse = await message.reply("sorry, you don't have permission to use that command.");

    } else if (!channelAllowed) {
        rejectionResponse = await message.reply('sorry, that command is not allowed in this channel.');

    } else if (params.length - 1 < minParams) {
        rejectionResponse = await message.reply('insufficient parameters!');

    } else if (com.maxParameters !== undefined && params.length - 1 > com.maxParameters) {
        rejectionResponse = await message.reply('too many parameters!');

    } else {
        if (!cooledDown) {
            const timeDif = utils.formatTimeDifference(new Date(), cooldownState[channelId]);
            const defaultMessage = 'that command has been used recently in this channel';
            const rejectionMessage = state.data.customCooldownResponses.get(com.command) ?? defaultMessage;
            rejectionResponse = await message.reply(
                `${rejectionMessage}, so I'll try to DM you instead.\n` +
                `You'll be able to use this command again in this channel **in ${timeDif}**.\n`);
            send = (body: string | Discord.MessageOptions) => utils.trySendDM(message.author, body);
        }

        if (com.cooldown && cooledDown) {
            const cooldownTime = new Date();
            cooldownTime.setMinutes(cooldownTime.getMinutes() + com.cooldown);
            cooldownState[channelId] = cooldownTime;
            state.data.commandCooldowns.set(com.command, cooldownState);
            state.data.customCooldownResponses.delete(com.command);
        }

        try {
            await com.execute(message, ...params.slice(1))(send);
        } catch (e) {
            utils.logError(e, 'Error executing command');
            await utils.tryOrLog(send('Something went wrong. Please try again.'), 'Error reporting command failure');
        }
    }

    await utils.deleteIfRejected(message, rejectionResponse);
}

export function channelIsAllowed(channelName: string, command: Bot.Command) {
    const state = State.getState();
    channelName = channelName.toLowerCase();

    if (command.allowedChannels) {
        return command.allowedChannels.has(channelName) || state.allowedChannels.has(channelName);
    }

    if (command.forbiddenChannels) {
        return !command.forbiddenChannels.has(channelName) && !state.forbiddenChannels.has(channelName);
    }

    // Should never be reached since one or the other must be defined
    return true;
}

export function hasPermission(user: Discord.User, command: Bot.Command) {
    const permissions = command.permissions;
    if (!permissions) {
        return true;
    }

    const userRoles = State.getState().server.member(user)?.roles.cache.array() || [];
    return userRoles.some(role => permissions.has(role.name.toLowerCase()));
}

import * as winston from 'winston';

import * as Bot from '../model/bot';
import { State } from '../model/state';
import { logError } from '../utils/error';

export const adminCommands: Bot.CommandSpec = {
    description: 'Admin commands',
    commands: [
        {
            command: 'migrate',
            description: 'Transfer all of a player\'s wins to a new name',
            parameters: ['fromName', 'toName'],
            execute: (_, fromName, toName) => async send => {
                const state = State.getState();

                try {
                    const newPlayer = await state.api.migratePlayer(fromName, toName);
                    await state.updateLeaderboard();

                    const winCount = newPlayer.roundList!.length;
                    const plural = winCount === 1 ? 'win' : 'wins';
                    await send(`Success. ${newPlayer.username} now has ${winCount} ${plural}.`);
                    winston.warn('Migrated a player', { fromName, toName });

                } catch (e) {
                    logError(e, 'Failed to migrate a player');
                    await handleError(send, e);
                }
            },
        },
    ],
};

async function handleError(send: Bot.SendMessageCallback, e: any) {
    if (e.statusCode) {
        if (e.statusCode === 404) {
            await send('Player not found.');
        } else if (e.statusCode === 400) {
            await send('Error: check your input and try again.');
        } else if (e.statusCode === 401) {
            await send('Auth failed. Check my config!');
        }
        return;
    }
    throw e;
}

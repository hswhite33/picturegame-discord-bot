export function getArchiveThreshold() {
    // Reddit archives threads that are over 6 months old
    const thresholdDate = new Date();
    thresholdDate.setUTCMonth(thresholdDate.getUTCMonth() - 6);
    return thresholdDate.valueOf() / 1000;
}

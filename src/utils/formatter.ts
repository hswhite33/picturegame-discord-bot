import * as Bot from '../model/bot';
import { NewRoundMessage } from '../reddit/MessageTypes';

import { cleanName, sanitizeWord } from './sanitizer';

export function formatPlayer(player: Bot.Player): string {
    const plural = player.winCount > 1 ? 'wins' : 'win';
    return `Rank ${player.rank}: **${player.name}**, ${player.winCount} ${plural}`;
}

export function formatPlayerList(players: Bot.Player[]): string {
    return players.map(p => {
        const plural = p.winCount > 1 ? 'wins' : 'win';
        return `${p.rank}: ${p.name}, ${p.winCount} ${plural}`;
    }).join('\n');
}

export function formatHelpText(spec: Bot.CommandSpec): string {
    let header = `**${spec.description}**\n\n`;

    if (spec.guideUrl) {
        header += `More info: <${spec.guideUrl}>\n\n`;
    }

    return header + spec.commands.map(c => {
        let output = `**!${c.command}**`;

        c.parameters?.forEach(param => {
            output += ` **<${param}>**`;
        });

        if (c.aliases && c.aliases.length > 0) {
            const plural = c.aliases.length === 1 ? 'alias' : 'aliases';
            output += ` *${plural}: ${c.aliases.join(', ')}*`;
        }

        if (c.permissions) {
            output += ` *(${[...c.permissions].join(', ')})*`;
        }

        output += `\n${c.description}`;

        return output;
    }).join('\n\n');
}

export function formatTimeDifference(from: Date, to: Date) {
    const diffMs = to.getTime() - from.getTime();

    const totalSecs = Math.floor(diffMs / 1000);
    const secs = totalSecs % 60;

    const totalMins = Math.floor(totalSecs / 60);
    const mins = totalMins % 60;

    const totalHours = Math.floor(totalMins / 60);

    const parts: string[] = [];
    if (totalHours) {
        parts.push(formatNumberWithLabel(totalHours, 'hour', 'hours'));
    }

    if (mins) {
        parts.push(formatNumberWithLabel(mins, 'minute', 'minutes'));
    }

    if (secs && totalMins < 10) {
        parts.push(formatNumberWithLabel(secs, 'second', 'seconds'));
    }

    return parts.join(', ');
}

export function formatNumberWithLabel(value: number, singular: string, plural: string) {
    if (value === 1) {
        return `1 ${singular}`;
    }
    return `${value} ${plural}`;
}

export function formatNewRoundSummary(round: NewRoundMessage) {
    return `**${sanitizeWord(round.title)}** by ${cleanName(round.host)}\n` +
        `<https://redd.it/${round.id}>\n` +
        round.url;
}

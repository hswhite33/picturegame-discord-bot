import * as winston from 'winston';

import { logError } from './error';

export function sleep(delay: number): Promise<void> {
    return new Promise(resolve => {
        setTimeout(resolve, delay);
    });
}

export function tryOrLog<T>(
    promise: Promise<T>,
    onFailedMessage: string,
    logFn: winston.LeveledLogMethod = winston.error): Promise<T | undefined> {

    return new Promise(res => {
        promise.then(res)
            .catch(e => {
                logError(e, onFailedMessage, logFn);
                res();
            });
    });
}

import * as Discord from 'discord.js';
import * as winston from 'winston';

import { SendMessageCallback } from '../model/bot';
import { State } from '../model/state';

import { tryOrLog } from './promise';
import { cleanName } from './sanitizer';
import { getUserInfoFromMember } from './user';

export async function setCurrentHostByMember(member: Discord.GuildMember) {
    winston.debug('Setting host by member... ' + member.user.username);

    const role = State.getState().getRole('Current Host');
    if (role) {
        await tryOrLog(member.roles.add(role), 'Failed to add role');
        winston.info('Host added', getUserInfoFromMember(member));
    } else {
        winston.error('Role not found, setting host failed');
    }
}

export async function replaceCurrentHostByName(rawUser: string, send?: SendMessageCallback) {
    winston.debug('Setting host by name... ' + rawUser);
    const user = rawUser.toLowerCase();

    const state = State.getState();
    const role = state.getRole('Current Host');
    const members = state.server.members.cache.array() ?? [];
    let toAdd: number = 0;
    let toRemove: number = 0;

    if (!role) {
        winston.error('Role not found, setting host failed');
        return;
    }

    const promises: Promise<any>[] = [];

    for (const member of members) {
        const username = member.user.username.toLowerCase();
        const nickname = member.nickname?.toLowerCase();

        if (user === username || user === nickname) {
            promises.push(tryOrLog(member.roles.add(role), 'Failed to add role'));
            winston.info('Adding host role to user', getUserInfoFromMember(member));
            toAdd++;

        } else if (member.roles.cache.has(role.id)) {
            promises.push(tryOrLog(member.roles.remove(role), 'Failed to remove role'));
            winston.info('Removing host role from user', getUserInfoFromMember(member));
            toRemove++;
        }
    }

    await Promise.all(promises);
    winston.info('Host role updated', {
        usersAdded: toAdd,
        usersRemoved: toRemove,
    });

    if (!toAdd) {
        winston.info('User not found', { rawUser });
        send = send ?? state.announce.bind(state) as SendMessageCallback;
        await send(`${cleanName(rawUser)} is the host but I couldn't set them here.`);
    }
}

export async function clearCurrentHost() {
    const state = State.getState();
    const role = state.getRole('Current Host');
    const members = state.server.members.cache.array() || [];

    if (!role) {
        winston.error('Role not found, clearing host failed');
        return;
    }

    const promises: Promise<any>[] = [];
    const players: string[] = [];

    for (const member of members) {
        if (member.roles.cache.find(r => r.name === 'Current Host')) {
            promises.push(tryOrLog(member.roles.remove(role), 'Failed to remove role'));
            players.push(member.user.username);
        }
    }

    await Promise.all(promises);
    winston.info('Host(s) removed', players);
}

export async function setDescription(id: string) {
    const newDescription = `Use this channel to discuss the current round: https://redd.it/${id}`;
    await tryOrLog(State.getState().channel.setTopic(newDescription), 'Failed to set channel description');
}

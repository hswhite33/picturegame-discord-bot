export * from './distance';
export * from './error';
export * from './formatter';
export * from './message';
export * from './promise';
export * from './rounds';
export * from './sanitizer';
export * from './time';
export * from './user';

String.prototype.truncate = function (length: number = 20) {
    if (this.length <= length) {
        return this;
    }
    return this.substring(0, length - 1) + '\u2026';
};

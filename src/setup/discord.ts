import * as Discord from 'discord.js';
import * as winston from 'winston';

import * as commands from '../commands';
import * as utils from '../utils';
import { tryOrLog } from '../utils/promise';

import { handleReaction } from '../model/reactions';
import { State } from '../model/state';

import * as config from '../../config.json';

const directMessageReply =
    `Hello, I'm ${config.discord.botName}! Use !help in the public chat to see a list of available commands.`;
const mentionReply = 'use !help to see a list of available commands.';

export function setupDiscord() {
    return new Promise((resolve) => {
        const state = State.getState();

        state.discord.on('ready', () => {
            winston.info('Ready to begin!');
            resolve();
        });

        state.discord.on('disconnect', () => {
            winston.warn('!!! Disconnected !!!');

            state.discord.destroy();
            state.login();
        });

        state.discord.on('reconnecting', () => {
            winston.info('Reconnecting....');
        });

        state.discord.on('debug', winston.debug);
        state.discord.on('error', e => utils.logError(e, 'Caught error'));

        const ignoredUsers = new Set<string>(config.bot.blockedUsers ?? []);

        state.discord.on('message', message => {
            if (ignoredUsers.has(message.author.id)) {
                winston.debug('Ignoring message from blocked user', utils.getUserInfoFromMessage(message));
            } else {
                tryOrLog(handleMessage(message), 'Error handling message');
            }
        });

        state.discord.on('guildMemberAdd', handleNewMember);

        state.discord.on('messageReactionAdd', onReaction);
        state.discord.on('messageReactionRemove', onReaction);

        state.login();
    });
}

async function handleMessage(message: Discord.Message) {
    if (message.author.bot) {
        return;
    }

    const channel = message.channel as Discord.TextChannel;

    if (channel.topic === undefined) {
        await message.reply(directMessageReply);
        return;
    }

    const prefix = config.discord.commandPrefix;
    const botUserId = State.getState().discord.user?.id;
    if (message.content[0] === prefix && !message.content.match(new RegExp(`^${prefix}+(?:\\s+|$)`))) {
        await commands.handleCommand(message, message.content.substring(config.discord.commandPrefix.length));
    } else if (botUserId && message.mentions.users.has(botUserId)) {
        await message.reply(mentionReply);
    }
}

async function handleNewMember(member: Discord.GuildMember) {
    const state = State.getState();
    const roundData = state.data.round;

    const welcomeMessage = `Welcome to the ${config.reddit.subreddit} Discord!` +
        'Please make sure your username or server nickname matches your Reddit account.';
    await tryOrLog(member.send(welcomeMessage), 'Failed to send welcome message');

    winston.info('New member added', utils.getUserInfoFromMember(member));

    if (!roundData.host) { return; }

    const currentHost = roundData.host.toLowerCase();
    const username = member.user.username.toLowerCase();
    const nickname = member.nickname?.toLowerCase();

    if (currentHost === username || currentHost === nickname) {
        await utils.setCurrentHostByMember(member);
    }
}

function onReaction(reaction: Discord.MessageReaction, user: Discord.User) {
    tryOrLog(handleReaction(reaction, user), 'Error occurred while handling reaction');
}

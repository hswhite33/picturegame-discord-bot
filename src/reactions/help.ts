import { categories, getNoCategoryText } from '../commands/meta';

import { CommandSpec, ReactionHandler } from '../model/bot';

import * as utils from '../utils';

interface HelpTextState {
    page: string | null;
}

export const helpReactionHandler: ReactionHandler<HelpTextState> = {
    type: 'help',
    emojis: () => ['◀'].concat([...categories.keys()]),
    init: () => {
        return { page: null };
    },
    update: (state, emoji) => {
        const newPage = emoji.name === '◀' ? null : emoji.name;
        const willUpdate = newPage !== state.page;
        state.page = newPage;
        return willUpdate;
    },
    getContents: state => {
        if (state.page === null) {
            return getNoCategoryText();
        }
        return utils.formatHelpText(categories.get(state.page) as CommandSpec);
    },
};

import * as fs from 'fs';

import { tryOrLog } from '../utils/promise';

import { PersistableState } from './bot';

const fileName = 'bot-state.json';

type RawMapToArray<K extends string | number, V> = Partial<Record<K, V[]>>;

interface RawPersistableState {
    madList?: string[];

    // Legacy naming: subscriptions to players' comments
    subscriptions?: RawMapToArray<string, string>;
    playerSubscriptions?: RawMapToArray<string, string>;

    // Subscriptions to hosts' comments
    roundSubscriptions_host?: RawMapToArray<string, string>;

    roundNotifications?: RawMapToArray<number, string>;
    playerNotifications?: RawMapToArray<string, string>;
}

function mapToObject<K extends string | number, V>(map: Map<K, Set<V>>): RawMapToArray<K, V> {
    const obj: RawMapToArray<K, V> = {};
    map.forEach((values, key) => {
        obj[key] = [...values];
    });
    return obj;
}

function convertToRaw(state: PersistableState): RawPersistableState {
    return {
        madList: state.madList,
        subscriptions: mapToObject(state.roundSubscriptions_players),
        playerSubscriptions: mapToObject(state.playerSubscriptions),
        roundSubscriptions_host: mapToObject(state.roundSubscriptions_host),
        roundNotifications: mapToObject(state.roundNotifications),
        playerNotifications: mapToObject(state.playerNotifications),
    };
}

function convertSubscriptionsMapFromRaw(rawMap?: RawMapToArray<string, string>) {
    const converted = new Map<string, Set<string>>();
    if (rawMap) {
        for (const id in rawMap) {
            converted.set(id, new Set(rawMap[id]));
        }
    }
    return converted;
}

function convertFromRaw(state: RawPersistableState): PersistableState {
    const roundNotifications = new Map<number, Set<string>>();
    if (state.roundNotifications) {
        for (const rawRoundNum in state.roundNotifications) {
            const roundNum = parseInt(rawRoundNum, 10);
            roundNotifications.set(roundNum, new Set(state.roundNotifications[rawRoundNum]));
        }
    }

    return {
        madList: state.madList ?? [],
        roundSubscriptions_players: convertSubscriptionsMapFromRaw(state.subscriptions),
        playerSubscriptions: convertSubscriptionsMapFromRaw(state.playerSubscriptions),
        roundSubscriptions_host: convertSubscriptionsMapFromRaw(state.roundSubscriptions_host),
        roundNotifications,
        playerNotifications: convertSubscriptionsMapFromRaw(state.playerNotifications),
    };
}

function getInitialPersistableState(): PersistableState {
    return {
        madList: [],
        roundSubscriptions_players: new Map<string, Set<string>>(),
        playerSubscriptions: new Map<string, Set<string>>(),
        roundSubscriptions_host: new Map<string, Set<string>>(),
        playerNotifications: new Map<string, Set<string>>(),
        roundNotifications: new Map<number, Set<string>>(),
    };
}

export function loadState(): PersistableState {
    if (!fs.existsSync(fileName)) {
        return getInitialPersistableState();
    }

    const data = fs.readFileSync(fileName, 'utf8');
    const rawState: RawPersistableState = JSON.parse(data);
    return convertFromRaw(rawState);
}

export function saveState(state: PersistableState): Promise<void> {
    const rawState = convertToRaw(state);

    return tryOrLog(new Promise((resolve, reject) => {
        fs.writeFile(fileName, JSON.stringify(rawState), 'utf8', (err) => {
            err ? reject(err) : resolve();
        });
    }), 'Failed to save state to filesystem');
}

interface String {
    truncate(length?: number): string;
}

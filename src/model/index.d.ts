declare module "*/config.json" {

    const api: {
        /**
         * The address of the PictureGame API.
         * It's recommended to use a local instance of the API for testing the bot. */
        apiUrl: string;

        /** The address of the UI for the API. */
        uiUrl: string;

        /**
         * Credentials for the API.
         * See the API's readme for details on creating these for yourself.
         * Currently only needed for !correct and !migrate commands
         */
        username?: string;
        password?: string;
    }

    const discord: {
        /** The ID of the discord server.
         * To obtain, turn dev tools on, right click on the server and select "Copy ID" */
        serverId: string;

        /** The ID of the discord channel the bot should post its automatic messages to. */
        channelId: string;

        /** The ID of the discord rules channel */
        rulesChannelId: string;

        /** The ID of the mod channel */
        modChannelId: string;

        /** The ID of the round-corrections channel */
        roundCorrectionsChannelId: string;

        /** The display name for the bot. Purely cosmetic. */
        botName: string;

        /** The string to be matched at the start of all commands, e.g. ! */
        commandPrefix: string;

        auth: {
            /** Discord auth token obtained from your oauth app. */
            token: string;
        };
    };

    const reddit: {
        /** Name of the subreddit to point the bot at.
         * Case-insensitive, but recommended to use correct case for cosmetic reasons.
         * Just the name, do not include /r/
         */
        subreddit: string;
    };

    const bot: {
        /** The maximum number of entries to display in !leaderboard, !stats and !rank */
        leaderboardSize: number;

        /** The maximum number of entries to display in !stats and !rank */
        maxStats: number;

        /** The number of minutes to keep grouping stats into the same message */
        statsCooldown: number;

        /** The minimum streak required to get a special announcement */
        streakThreshold: number;

        /** The minimum win gap required to get a special announcement */
        gapThreshold: number;

        log: {
            /** The level of logging to be output. */
            level: 'debug' | 'info' | 'warn' | 'error';

            /** Whether to write log lines to the console. */
            console: boolean;

            /** Whether to log to ElasticSearch */
            enableEs?: boolean;

            /**
             * Index name to use with ElasticSearch.
             * Must be provided if enableEs is true.
             */
            esIndexName?: string;

            /**
             * Server details for ElasticSearch.
             * Defaults to 127.0.0.1:9200
             */
            esHost?: string;
            esPort?: number;

            /** Maximum number of log entries to buffer before flushing to ES (default 512) */
            esMaxBufferSize?: number;

            /** Maximum number of seconds before flushing logs to ES (default 4) */
            esFlushMaxDelaySeconds?: number;

            /** Specification for posting log messages to Discord channels */
            channelLogging?: ChannelLoggingSpec[];
        };

        /** UDP address and port to listen on for communication from the Reddit bot */
        address: string;
        port: number;

        /**
         * Config on a per-command basis.
         * Any commands not included here will use default settings.
         */
        commands: CommandConfig[];

        /** List of channels that all commands can be used in unless explicitly forbidden. */
        globalAllowedChannels?: string[];

        /** List of channels that no commands can be used in unless explicitly allowed. */
        globalForbiddenChannels?: string[];

        /** List of users to ignore modmail from */
        ignoreModmailAuthors?: string[];

        /** List of modmail subjects to ignore */
        ignoreModmailSubjects?: string[];

        /** List of user ids to ignore messages from */
        blockedUsers?: string[];

        /** List of urls to gifs to select from when using !celebrate */
        celebrateGifs?: string[];
    };

    interface ChannelLoggingSpec {
        /** The id of the channel to write log messages to */
        channelId: string;

        /** The name of the service from which to print logs (picturegame-bot, picturegame-discord-bot, picturegame-api) */
        serviceName: string;

        /** The minimum log level to include in the output (debug, info, warn, error, fatal) */
        level: string;
    }

    interface CommandConfig {
        /** Name of the command. Matched against Command.command (e.g. current, leaderboard). Case sensitive */
        name: string;

        /** List of roles allowed to use the command. If blank, anyone can use it. */
        roles?: string[];

        /** List of alternative names for the command. */
        aliases?: string[];

        /** Minimum number of minutes that must pass between uses of the command in the same channel */
        cooldown?: number;

        /**
         * List of channels that the command can be used in. Channel names, case insensitive.
         * If not provided, all channels are allowed unless explicitly forbidden (below).
         * Only one of allowedChannels or forbiddenChannels should be provided on a command.
         * If provided, a channel must either be in allowedChannels or globallAllowedChannels.
         */
        allowedChannels?: string[];

        /**
         * List of channels that the command cannot be used in. Channel names, case insensitive.
         * If not provided, no channels are explicitly forbidden.
         * Only one of allowedChannels or forbiddenChannels should be provided on a command.
         * If provided, a channel must not be in forbiddenChannels or globalForbiddenChannels.
         */
        forbiddenChannels?: string[];
    }
}

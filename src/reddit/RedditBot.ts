import * as discord from 'discord.js';
import * as winston from 'winston';

import { State } from '../model/state';

import { formatNewRoundSummary, formatTimeDifference } from '../utils/formatter';
import { writeLogToDiscord } from '../utils/logging';
import { MessageCharacterLimit } from '../utils/message';
import { tryOrLog } from '../utils/promise';
import { replaceCurrentHostByName, setDescription } from '../utils/rounds';
import { cleanName, sanitizeWord } from '../utils/sanitizer';

import * as config from '../../config.json';

import * as Messages from './MessageTypes';
import * as Subscriptions from './Subscription';

const RedditBotServiceName = 'picturegame-bot';

export function onReceiveMessage(message: Messages.RedditBotMessage) {
    if (Messages.isRoundStatus(message)) {
        return onReceiveRoundStatus(message);
    }
    if (Messages.isNewComment(message)) {
        return Subscriptions.onNewComment(message);
    }
    if (Messages.isCommentEdit(message)) {
        return Subscriptions.onCommentEdit(message);
    }
    if (Messages.isModPost(message)) {
        return onModPost(message);
    }
    if (Messages.isModmail(message)) {
        return onModMail(message);
    }
    if (Messages.isRoundCorrection(message)) {
        return onRoundCorrection(message);
    }
    if (Messages.isLogMessage(message)) {
        return onLogMessage(message);
    }
    winston.warn('Unrecognized message from Reddit bot', message);
    return Promise.resolve();
}

function onReceiveRoundStatus(message: Messages.CurrentRoundMessage) {
    tryOrLog(Subscriptions.onRoundStatus(message), 'Failed to notify players about round status');

    if (Messages.isNewRound(message)) {
        return onNewRound(message);
    }
    if (Messages.isSolvedRound(message)) {
        return onRoundSolved(message);
    }
    if (Messages.isAbandonedRound(message)) {
        return onRoundAbandoned(message);
    }
    winston.warn('Unrecognized round status from Reddit bot', message);
    return Promise.resolve();
}

async function onNewRound(message: Messages.NewRoundMessage) {
    const state = State.getState();
    const title = sanitizeWord(message.title);

    state.data.round = {
        id: message.id,
        pic: message.url,
        title,
        host: message.host,
        status: 'unsolved',
        roundNumber: message.roundNumber,
        timeStarted: new Date(message.postTime * 1000),
    };

    await Promise.all([
        replaceCurrentHostByName(message.host),
        state.announce('@here, a new round is up:\n\n' + formatNewRoundSummary(message)),
        setDescription(message.id),
        Subscriptions.subscribeCurrentFromNewRound(message.host),
    ]);

    state.startCooldown('current', 'the current round has only just started');
    winston.info('New round up', state.data.round);
}

async function onRoundSolved(message: Messages.SolvedRoundMessage) {
    const state = State.getState();
    const round = state.data.round;

    const timeSolved = new Date(message.winTime * 1000);
    round.timeSolved = timeSolved;

    round.status = 'over';
    round.winner = cleanName(message.winner);

    state.resetCooldowns('current');
    await Promise.all([
        state.updateLeaderboard(),
        Subscriptions.unsubscribeCurrent(),
    ]);

    winston.info('Round won');

    let response: string;

    if (message.winner && message.winCount && message.commentId) {
        const roundLength = round.timeStarted && formatTimeDifference(round.timeStarted, timeSolved);

        response = roundLength
            ? `@here, the latest round was solved in ${roundLength} by **${round.winner}**!`
            : `@here, the latest round was solved by **${round.winner}**!`;

        if (message.winCount === 1) {
            response += '\n*It\'s their first win! Make them feel welcome.*';
        } else if (message.winCount === 50 || message.winCount % 100 === 0) {
            response += `\n:tada:*It's their ${message.winCount}th win! Congrats!*:tada:`;
        }
        if (message.streak >= config.bot.streakThreshold) {
            response += `\n*They are on a winning streak of ${message.streak} rounds!*`;
        }
        if (message.gap >= config.bot.gapThreshold) {
            response += `\n*It's their first win in ${message.gap} rounds! Welcome back!*`;
        }
        response += `\n\nWinning comment: <https://reddit.com/comments/${round.id}/-/${message.commentId}>`;
    } else {
        response = '@here, the latest round was solved!';
    }

    await state.announce(response);
}

async function onRoundAbandoned(message: Messages.AbandonedRoundMessage) {
    const state = State.getState();
    state.data.round.status = message.status;
    state.resetCooldowns('current');


    const tasks: Promise<any>[] = [state.announce(`@here, the latest round was ${message.status}.`)];
    if (message.status !== Messages.RoundStatus.Deleted) {
        // If it's been deleted, we want to stay subscribed when it gets reposted
        tasks.push(Subscriptions.unsubscribeCurrent());
    }
    await Promise.all(tasks);

    winston.info(`Round ${message.status}`);
}

async function onModPost(_message: Messages.ModPostMessage) {
    // TODO
}

async function onModMail(modmail: Messages.ModMailMessage) {
    const state = State.getState();
    if (state.ignoreModmailAuthors.has(modmail.author.toLowerCase())) {
        return;
    }
    if (state.ignoreModmailSubjects.has(modmail.subject.toLowerCase())) {
        return;
    }

    const channel = state.server.channels.cache.get(config.discord.modChannelId);
    if (!channel) { return; }

    const author = cleanName(modmail.author);
    const messageHeader = `**New Modmail message from ${author} in conversation *${modmail.subject}***`;
    const messageLink = `https://mod.reddit.com/mail/_/${modmail.conversation}`;

    let message = messageHeader + '\n\n' + modmail.body + '\n\n' + messageLink;
    if (message.length > MessageCharacterLimit) {
        message = messageHeader + '\n\n' + messageLink;
    }
    await (channel as discord.TextChannel).send(message);
}

async function onRoundCorrection(message: Messages.RoundCorrectionMessage) {
    const state = State.getState();
    const channel = state.server.channels.cache.get(config.discord.roundCorrectionsChannelId) as discord.TextChannel;
    if (!channel) { return; }

    const { roundNumber, winner, commentId, submissionId } = message;
    const url = `https://reddit.com/comments/${submissionId}/-/${commentId}`;
    let announcement: string;

    if (Messages.isPendingRoundCorrection(message)) {
        winston.info('New pending round correction', message);
        const { correcter } = message;
        announcement =
            `${cleanName(correcter)} has +corrected **${cleanName(winner)}** on Round ${roundNumber}\n\n` +
            'Mods/OP: Visit this comment and +correct it again to confirm.\n' +
            `<${url}>`;

        await channel.send(announcement);
        return;
    }

    await state.updateLeaderboard();

    const round = state.data.round;
    if (round.roundNumber === roundNumber) {
        round.winner = winner;
        round.timeSolved = new Date((message as Messages.CompletedRoundCorrectionMessage).winTime * 1000);
    }

    const previousAnnouncement = await tryOrLog(getMessageForUrl(channel, url), 'Failed to get message for url');
    winston.info('Round correction verified', message);
    if (previousAnnouncement) {
        await previousAnnouncement.react('✅');
        return;
    }

    winston.warn('Unable to find a message relating to the given url', { url });
    announcement =
        `**${cleanName(winner)}** has been awarded the win for Round ${roundNumber}.\n` +
        `<${url}>`;
    await channel.send(announcement);
}

async function getMessageForUrl(channel: discord.TextChannel, url: string) {
    const messages = await channel.messages.fetch();
    for (const [, message] of messages) {
        if (message.content.indexOf(url) > -1) {
            return message;
        }
    }
}

async function onLogMessage(message: Messages.LogMessage) {
    await writeLogToDiscord(RedditBotServiceName, message.level, message.message, message.data);
}
